
Test SW to send CAN/OBD queries.

Based on ESP-IDF CAN master example


## Hardware
- ESP32-WROOM-32
- SN65HVD230 (or any other CAN transceiver IC)

### Connections
- IO 22 -> CAN RX
- IO 21 -> CAN TX

### Supported PIDS and Services
Service 01	
PID (hex)	
0x00	PIDs supported = 0x00188000
0x0C	Engine RPM
0x0D	Vehicle Speed
	
	
Service 0x30	
PID (hex)	
0x00	PIDs supported = 0xFFF80000
0x01	Interface Version
0x02	Battery Voltage
0x03	Throttle Position
0x04	Motor RPM
0x05	Wheel rotation time
0x06	Wheel size code
0x07	Motor Power
0x08	Assist Level
0x09	Motor Power
0x0A	Max speed
0x0B	Max battery capacity
0x0C	Max charger current
0x0D	Battery type
	
Service 0x31	
PID (hex)	
0x00	PIDs supported = 0xFFFF8000
0x01	Battery Current
0x02	Charging Current
0x03	U24 Voltage
0x04	Temp Cell1
0x05	Temp Cell2
0x06	Temp Motor
0x07	Temp BLDC
0x08	Motor Status
0x09	Power Status
0x0A	BLDC Status
0x0B	DRIVE FW version
0x0C	ODO
0x0D	TRIP
0x0E	Charger Status
0x0F	Battery Capacity
0x10	Remaining distance
0x11	remaining time for charging
	
	
Service 0x32	
PID (hex)	
0x00	PIDs supported = 0xFFFFF000
0x01	Set Assist Level = 1
0x02	Set Assist Level = 2
0x03	Set Assist Level = 3
0x04	Set Assist Level = 4
0x05	Set Assist Level = 5
0x06	Reset ODO
0x07	Reset TRIP
0x08	Set wheel size code 8
0x09	Set wheel size code 16
0x0A	Set wheel size code 20
0x0B	Set wheel size code 28
0x0C	Set wheel size code 30
0x0D	Set max speed 20
0x0E	Set max speed 25
0x0F	Set max speed 30
0x10	Set max speed 35
0x11	Set max speed 40
0x12	Set max battery capacity
0x13	Set max charger current
0x14	Set battery type
	

