/* CAN Network Master Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/*
 * The following example demonstrates a master node in a CAN network. The master
 * node is responsible for initiating and stopping the transfer of data messages.
 * The example will execute multiple iterations, with each iteration the master
 * node will do the following:
 * 1) Start the CAN driver
 * 2) Repeatedly send ping messages until a ping response from slave is received
 * 3) Send start command to slave and receive data messages from slave
 * 4) Send stop command to slave and wait for stop response from slave
 * 5) Stop the CAN driver
 */
#include <stdio.h>
#include <stdlib.h>
#include <linenoise/linenoise.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "esp_err.h"
#include "esp_log.h"
#include "driver/can.h"
#include "driver/uart.h"
#include "esp_console.h"
#include "esp_vfs_dev.h"

/* --------------------- Definitions and static variables ------------------ */
//Example Configuration
#define PING_PERIOD_MS          250
#define NO_OF_DATA_MSGS         50
#define NO_OF_ITERS             3
#define ITER_DELAY_MS           1000
#define RX_TASK_PRIO            8
#define TX_TASK_PRIO            9
#define CTRL_TSK_PRIO           10
#define TX_GPIO_NUM             21
#define RX_GPIO_NUM             22
#define EXAMPLE_TAG             "CAN Test Master"

#define ID_MASTER_STOP_CMD      0x0A0
#define ID_MASTER_START_CMD     0x0A1
#define ID_MASTER_PING          0x0A2
#define ID_SLAVE_STOP_RESP      0x0B0
//#define ID_SLAVE_DATA           0x0B1
#define ID_SLAVE_DATA           0x07E8
#define ID_SLAVE_PING_RESP      0x0B2
#define ID_MASTER_OBD_Q         0x7E0

typedef enum {
    TX_SEND_PINGS,
    TX_SEND_START_CMD,
    TX_SEND_STOP_CMD,
    TX_TASK_EXIT,
} tx_task_action_t;

typedef enum {
    RX_RECEIVE_PING_RESP,
    RX_RECEIVE_DATA,
    RX_RECEIVE_STOP_RESP,
    RX_TASK_EXIT,
} rx_task_action_t;

static const can_timing_config_t t_config = CAN_TIMING_CONFIG_250KBITS();
static const can_filter_config_t f_config = CAN_FILTER_CONFIG_ACCEPT_ALL();
static const can_general_config_t g_config = CAN_GENERAL_CONFIG_DEFAULT(TX_GPIO_NUM, RX_GPIO_NUM, CAN_MODE_NORMAL);

static const can_message_t ping_message = {.identifier = ID_MASTER_PING, .data_length_code = 0,
                                           .flags = CAN_MSG_FLAG_SS,  .data = {0, 0 , 0 , 0 ,0 ,0 ,0 ,0}};
static const can_message_t start_message = {.identifier = ID_MASTER_START_CMD, .data_length_code = 0,
                                            .flags = CAN_MSG_FLAG_NONE, .data = {0, 0 , 0 , 0 ,0 ,0 ,0 ,0}};
static const can_message_t oma_speed_message = {.identifier = ID_MASTER_OBD_Q, .data_length_code = 8,
                                            .flags = CAN_MSG_FLAG_SS, .data = {3, 0x01 , 0x0D , 0 ,0 ,0 ,0 ,0}};
static const can_message_t oma_battery_voltage_message = {.identifier = ID_MASTER_OBD_Q, .data_length_code = 8,
                                            .flags = CAN_MSG_FLAG_SS, .data = {3, 0x30 , 0x02 , 0 ,0 ,0 ,0 ,0}};
static const can_message_t oma_charging_current_message = {.identifier = ID_MASTER_OBD_Q, .data_length_code = 8,
                                            .flags = CAN_MSG_FLAG_SS, .data = {3, 0x31 , 0x02 , 0 ,0 ,0 ,0 ,0}};
static const can_message_t oma_battery_temperature_message = {.identifier = ID_MASTER_OBD_Q, .data_length_code = 8,
                                            .flags = CAN_MSG_FLAG_SS, .data = {3, 0x31 , 0x04 , 0 ,0 ,0 ,0 ,0}};
static const can_message_t oma_battery_temperature2_message = {.identifier = ID_MASTER_OBD_Q, .data_length_code = 8,
                                            .flags = CAN_MSG_FLAG_SS, .data = {3, 0x31 , 0x05 , 0 ,0 ,0 ,0 ,0}};
static const can_message_t oma_battery_capacity_message = {.identifier = ID_MASTER_OBD_Q, .data_length_code = 8,
                                            .flags = CAN_MSG_FLAG_SS, .data = {3, 0x31 , 0x0F , 0 ,0 ,0 ,0 ,0}};
static const can_message_t oma_battery_charger_status_message = {.identifier = ID_MASTER_OBD_Q, .data_length_code = 8,
                                            .flags = CAN_MSG_FLAG_SS, .data = {3, 0x31 , 0x0E , 0 ,0 ,0 ,0 ,0}};
static const can_message_t oma_battery_charging_limits_message = {.identifier = ID_MASTER_OBD_Q, .data_length_code = 8,
                                            .flags = CAN_MSG_FLAG_SS, .data = {3, 0x01 , 0x0D , 0 ,0 ,0 ,0 ,0}};
static const can_message_t stop_message = {.identifier = ID_MASTER_STOP_CMD, .data_length_code = 0,
                                           .flags = CAN_MSG_FLAG_NONE, .data = {0, 0 , 0 , 0 ,0 ,0 ,0 ,0}};
static can_message_t oma_open_message = {.identifier = ID_MASTER_OBD_Q, .data_length_code = 8,
                                           .flags = CAN_MSG_FLAG_SS, .data = {3, 0x31 , 0x02 , 0 ,0 ,0 ,0 ,0}};
static can_message_t oma_write_open_message = {.identifier = ID_MASTER_OBD_Q, .data_length_code = 8,
                                           .flags = CAN_MSG_FLAG_SS, .data = {7, 0x31 , 0x02 , 0 ,0 ,0 ,0 ,0}};

static QueueHandle_t tx_task_queue;
static QueueHandle_t rx_task_queue;
static SemaphoreHandle_t stop_ping_sem;
static SemaphoreHandle_t ctrl_task_sem;
static SemaphoreHandle_t done_sem;
int serviceReq =0;
int pidReq = 0;
long dataWriteReq =0 ;

/* --------------------------- Tasks and Functions -------------------------- */

float decodeOBD01(uint8_t arvo[])
{
	//printf("Decoding received Mode 01 PID 0x%02x\n", pidReq);
	switch (pidReq)
	{
        case 0x00:  // supported PIDs
			printf("Supported PIDS: 0x%02x", arvo[3]);
			printf("%02x", arvo[4]);
			printf("%02x", arvo[5]);
			printf("%02x\n", arvo[6]);
			printf("=============================================\n");
            return (arvo[3] << 24 ) | (arvo[4] << 16) |  (arvo[5] << 8 ) | arvo[6];
            break;
		case 0x0C: // engine rpm			        
            return ((float)(256*arvo[3]+arvo[4]))/4;			
			break;
		case 0x0D: // vehicle speed			        
            return (arvo[3]);			
			break;
        default:
            printf("Unsupported mode");
	}
    return 0;
}
float decodeOBD30(uint8_t arvo[])
{
	//printf("Decoding received Mode 30 PID 0x%02x\n", pidReq);
	switch (pidReq)
	{
        case 0x00:  // supported PIDs
			printf("Supported PIDS: 0x%02x", arvo[3]);
			printf("%02x", arvo[4]);
			printf("%02x", arvo[5]);
			printf("%02x\n", arvo[6]);
			printf("=============================================\n");
            return (arvo[3] << 24 ) | (arvo[4] << 16) |  (arvo[5] << 8 ) | arvo[6];
            break;
		case 0x01: // interface version			        
            return (arvo[3]);			
			break;
		case 0x02: // battery voltage			       
            return ((float)(256*arvo[3]+arvo[4])/100);			
			break;
		case 0x03: // throttle position			        
            return (arvo[3]);			
			break;
		case 0x04: // motor rpm			        
            return (float)(256*arvo[3]+arvo[4]);			
			break;
		case 0x05: // wheel rotation time			        
            return (float)(256*arvo[3]+arvo[4]);		
			break;
		case 0x06: // wheel size code			        
            return (arvo[3]);			
			break;
 		case 0x07: // motor power			        
            return (float)(256*arvo[3]+arvo[4])-8000;			
			break;       
 		case 0x08: // assist level			        
            return (arvo[3]);			
			break;       
		case 0x09: // motor power2 pros			        
            return (arvo[3]-100);			
			break;
		case 0x0A: // max speed			        
            return (arvo[3]);			
			break;
		case 0x0B: // max battery capacity			        
            return (float)(256*arvo[3]+arvo[4]);		
			break;
		case 0x0C: // max charger current			        
            return (arvo[3]);			
			break;
		case 0x0D: // battery type			        
            return (arvo[3]);			
			break;
        default:
            printf("Unsupported mode");
	}
    return 0;
}
float decodeOBD31(uint8_t arvo[])
{
	switch (pidReq)
	{
        case 0x00:  // supported PIDs
			printf("Supported PIDS: 0x%02x", arvo[3]);
			printf("%02x", arvo[4]);
			printf("%02x", arvo[5]);
			printf("%02x\n", arvo[6]);
			printf("=============================================\n");
            return (arvo[3] << 24 ) | (arvo[4] << 16) |  (arvo[5] << 8 ) | arvo[6];
            break;
		case 0x01: // battery current			
            return (float)(256*arvo[3]+arvo[4]);			
			break;            
		case 0x02: // charging current			
            return ((float)(256*arvo[3]+arvo[4])/10);			
			break;
		case 0x03: // U24 voltage
            return ((float)(256*arvo[3]+arvo[4])/10);			
			break;
		case 0x04: // battery temp cell1
            return (arvo[3]);			
			break;
		case 0x05: // battery temp cell2	        
            return (arvo[3]);			
			break;
		case 0x06: // motor temp 
            return (arvo[3]);			
			break;
		case 0x07: // bldc temp	        
            return (arvo[3]);			
			break;   
		case 0x0B: // Drive fw version            		
            return ((float)(256*arvo[3]+arvo[4]))/100;			
			break; 
		case 0x0C: // odo2	        
            return (arvo[3] << 24 ) | (arvo[4] << 16) |  (arvo[5] << 8 ) | arvo[6];			
			break;   
		case 0x0D: // trip	        
            return (arvo[3] << 24 ) | (arvo[4] << 16) |  (arvo[5] << 8 ) | arvo[6];			
			break;
		case 0x0E: // battery charger status
            return (arvo[3] << 24 ) | (arvo[4] << 16) |  (arvo[5] << 8 ) | arvo[6];			
			break;
		case 0x0F: // battery capacity %	
            return (arvo[3]);			
			break;
		case 0x10: // remaining distance			
            return (float)(256*arvo[3]+arvo[4]);			
			break;   
		case 0x11: // remaining charge time			
            return (float)(256*arvo[3]+arvo[4]);			
			break;            
        default:
            printf("Unsupported mode");
	}
    return 0;
}

float decodeOBD32(uint8_t arvo[])
{

	switch (pidReq)
	{
        case 0x00:  // supported PIDs
			printf("Supported PIDS: 0x%02x", arvo[3]);
			printf("%02x", arvo[4]);
			printf("%02x", arvo[5]);
			printf("%02x\n", arvo[6]);
			printf("=============================================\n");
            return (arvo[3] << 24 ) | (arvo[4] << 16) |  (arvo[5] << 8 ) | arvo[6];
            break;
		case 0x12: // set max bat cap			
            return (float)(256*arvo[3]+arvo[4]);
        default:
            printf("Unsupported mode");
	}
    return 0;
}
float decodeOBD33(uint8_t arvo[])
{
	switch (pidReq)
	{
        case 0x00:  // supported PIDs
			printf("Supported PIDS: 0x%02x", arvo[3]);
			printf("%02x", arvo[4]);
			printf("%02x", arvo[5]);
			printf("%02x\n", arvo[6]);
			printf("=============================================\n");
            return (arvo[3] << 24 ) | (arvo[4] << 16) |  (arvo[5] << 8 ) | arvo[6];
            break;
		case 0x01: // get rpm ratio			
            return (arvo[3] << 24 ) | (arvo[4] << 16) |  (arvo[5] << 8 ) | arvo[6];				
			break;            
		case 0x02: // set rpm ratio	, to be done	
            return ((float)(256*arvo[3]+arvo[4])/10);			
			break;          
        default:
            printf("Unsupported mode");
	}
    return 0;
}
static void can_receive_task(void *arg)
{
    while (1) {
        rx_task_action_t action;
        xQueueReceive(rx_task_queue, &action, portMAX_DELAY);
        //printf("Jonoon tuli jotain dataa, %d\n",action);
        if (action == RX_RECEIVE_PING_RESP) {
            //Listen for ping response from slave
            while (1) {
                can_message_t rx_msg;
                can_receive(&rx_msg, portMAX_DELAY);
                if (rx_msg.identifier == ID_SLAVE_PING_RESP) {
                    xSemaphoreGive(stop_ping_sem);
                    xSemaphoreGive(ctrl_task_sem);
                    break;
                }
            }
        } else if (action == RX_RECEIVE_DATA) {
            //Receive data messages from slave
            uint32_t data_msgs_rec = 0;
            while (data_msgs_rec < NO_OF_DATA_MSGS) {
                can_message_t rx_msg;
                can_receive(&rx_msg, portMAX_DELAY);
                if (rx_msg.identifier == ID_SLAVE_DATA) {
                    uint32_t data = 0;
                    for (int i = 0; i < rx_msg.data_length_code; i++) {
                        data |= (rx_msg.data[i] << (i * 8));
                    }
                    if (serviceReq == 0x01)
                    {
                        float vastaus =decodeOBD01(rx_msg.data);
                        ESP_LOGI(EXAMPLE_TAG, "\nDecoded value (x01):  %.2f\n", vastaus);
                    }
                    else if (serviceReq == 0x30)
                    {
                        float vastaus =decodeOBD30(rx_msg.data);
                        ESP_LOGI(EXAMPLE_TAG, "Decoded value (x30):  %.2f\n", vastaus);
                    }

                    else if (serviceReq== 0x31)
                    {
                        float vastaus =decodeOBD31(rx_msg.data);
                        ESP_LOGI(EXAMPLE_TAG, "Decoded value (x31):  %.3f\n", vastaus);
                    }
                    else if (serviceReq == 0x32)
                    {
                        float vastaus =decodeOBD32(rx_msg.data);
                        ESP_LOGI(EXAMPLE_TAG, "\nDecoded value (x32):  %.2f\n", vastaus);
                    }
                    else if (serviceReq == 0x33)
                    {
                        float vastaus =decodeOBD33(rx_msg.data);
                        ESP_LOGI(EXAMPLE_TAG, "\nDecoded value (x33):  %.2f\n", vastaus);
                    }
                    data_msgs_rec ++;
                    break;
                }
            }
            xSemaphoreGive(ctrl_task_sem);
        } else if (action == RX_RECEIVE_STOP_RESP) {
            //Listen for stop response from slave
            while (1) {
                can_message_t rx_msg;
                can_receive(&rx_msg, portMAX_DELAY);
                if (rx_msg.identifier == ID_SLAVE_STOP_RESP) {
                    xSemaphoreGive(ctrl_task_sem);
                    break;
                }
            }
        } else if (action == RX_TASK_EXIT) {
            break;
        }
    }
    vTaskDelete(NULL);
}

static void can_transmit_task(void *arg) {

    char buf0[2];
    char buf[2];
    char buf1[5];
    char buf2[5];
    char buf3[11];
    ESP_ERROR_CHECK(uart_driver_install(CONFIG_CONSOLE_UART_NUM,256,0,0,NULL,0));
    esp_vfs_dev_uart_use_driver(CONFIG_CONSOLE_UART_NUM);
    setvbuf(stdin, NULL, _IONBF,0);
    setvbuf(stdout, NULL, _IONBF,0);
    esp_vfs_dev_uart_set_rx_line_endings(ESP_LINE_ENDINGS_CR);
    esp_vfs_dev_uart_set_tx_line_endings(ESP_LINE_ENDINGS_CRLF);
    
    while (1) {
        tx_task_action_t action;
        vTaskDelay(200 / portTICK_PERIOD_MS);
        xQueueReceive(tx_task_queue, &action, portMAX_DELAY);
 
        printf("\nPress enter to continue\n");
        fgets(buf0, 3, stdin);

        if (action == TX_SEND_PINGS) {
            //Repeatedly transmit pings
            ESP_LOGI(EXAMPLE_TAG, "Transmitting ping");
            while (xSemaphoreTake(stop_ping_sem, 0) != pdTRUE) {
                can_transmit(&ping_message, portMAX_DELAY);
                vTaskDelay(pdMS_TO_TICKS(PING_PERIOD_MS));
            }
        } else if (action == TX_SEND_START_CMD) {
            //Transmit action to slave
            // can_transmit(&start_message, portMAX_DELAY);
            // ESP_LOGI(EXAMPLE_TAG, "Transmitted start commancan_message_t
            linenoiseClearScreen();
            printf("\n\n\tSelect the service \n\n");
            printf("1. Read battery voltage\n");
            printf("2. Read charging current\n");    
            printf("3. Read battery temperature\n");
            printf("4. Read battery temperature 2nd cell\n");
            printf("5. Read remaining battery capacity \n");
            printf("6. Read charger status\n");
            printf("7. Read min and max charging levels\n");
            printf("8. Send query with Service and Pid \n");
            printf("9. Set a parameter (0x32). Service, Pid and data\n");

            
            fgets(buf, 2, stdin);
            if (atoi(buf)==8)
                {
                    printf("\n Option selected: %s\n\n",buf);
                    printf("  Enter Service: > (for ex 0x31)\n");
                    fgets(buf1, 5, stdin);
                    printf("  Enter Pid:     > (for ex 0x0F)\n");
                    fgets(buf2, 5, stdin);
                    printf("\n Selected %s:%s \n\n", buf1, buf2);
                    serviceReq = (int)strtol(buf1, NULL, 16);
                    pidReq = (int)strtol(buf2, NULL, 16);
                    oma_open_message.data[1]=serviceReq;
                    oma_open_message.data[2]=pidReq;
                }
            else if (atoi(buf)==9)
            {
                    printf("\n Option selected: %s\n\n",buf);
                    printf("  Enter Service: > (for ex 0x32)\n");
                    fgets(buf1, 5, stdin);
                    printf("  Enter Pid:     > (for ex 0x12)\n");
                    fgets(buf2, 5, stdin);
                    printf("  Enter value and press enter: > (for ex 1234)\n");
                    fgets(buf3, 11, stdin);
                    printf("\n Selected %s:%s:%s \n\n", buf1, buf2, buf3);
                    serviceReq = (int)strtol(buf1, NULL, 16);
                    printf("serviceReq: %02x\n",serviceReq);
                    pidReq = (int)strtol(buf2, NULL, 16);
                    dataWriteReq = (long)strtol(buf3, NULL, 10);
                    printf("  dataWritereq: > %ld\n", dataWriteReq);
                    oma_open_message.data[1]=serviceReq;
                    oma_open_message.data[2]=pidReq;
                    if ((pidReq == 18)&&(serviceReq == 50)){
                        oma_open_message.data[3]=(uint8_t)((long)dataWriteReq/256); 
                        oma_open_message.data[4]=(uint8_t)((long)dataWriteReq%256);
                    }
                    //
                    else if ((pidReq == 2)&&(serviceReq == 51))
                    {
                        oma_open_message.data[3]=(uint8_t)((long)dataWriteReq/16777216); 
                        oma_open_message.data[4]=(uint8_t)(((long)dataWriteReq%16777216)/65536);
                        oma_open_message.data[5]=(uint8_t)(((long)dataWriteReq%65536)/256);
                        oma_open_message.data[6]=(uint8_t)((long)dataWriteReq%256);
                    }
                    else {
                        oma_open_message.data[3]=(uint8_t)dataWriteReq; 
                    }
            }
            else 
                printf("\n Option selected: %s\n\n",buf);            

            switch (atoi(buf))
            {
            case 1:
                can_transmit(&oma_battery_voltage_message, portMAX_DELAY);
                serviceReq =0x30;
                pidReq = 0x02;
                ESP_LOGI(EXAMPLE_TAG, "Transmitted read-battery-voltage command");
                break;            
            case 2:
                can_transmit(&oma_charging_current_message, portMAX_DELAY);
                serviceReq =0x31;
                pidReq = 0x02;
                ESP_LOGI(EXAMPLE_TAG, "Transmitted read-charging-current command");
                break;
            case 3:
                can_transmit(&oma_battery_temperature_message, portMAX_DELAY);
                serviceReq =0x31;
                pidReq = 0x04;
                ESP_LOGI(EXAMPLE_TAG, "Transmitted read-battery-temperature command");
                break;
            case 4:
                can_transmit(&oma_battery_temperature2_message, portMAX_DELAY);
                serviceReq =0x31;
                pidReq = 0x05;
                ESP_LOGI(EXAMPLE_TAG, "Transmitted read-battery-temperature2 command");
                break;
            case 5:
                can_transmit(&oma_battery_capacity_message, portMAX_DELAY);
                serviceReq =0x31;
                pidReq = 0x0f;
                ESP_LOGI(EXAMPLE_TAG, "Transmitted read-battery-capacity command");
                break;
            case 6:
                can_transmit(&oma_battery_charger_status_message, portMAX_DELAY);
                serviceReq =0x31;
                pidReq = 0x0e;
                ESP_LOGI(EXAMPLE_TAG, "Transmitted read-charger-status command");
                break;
            case 7:
                can_transmit(&oma_battery_charging_limits_message, portMAX_DELAY);
                ESP_LOGI(EXAMPLE_TAG, "Transmitted read-battery-charging-limits command");
                break;
            case 8:
                can_transmit(&oma_open_message, portMAX_DELAY);
                ESP_LOGI(EXAMPLE_TAG, "Transmitted open query\n");
                break;
            case 9 :
                can_transmit(&oma_open_message, portMAX_DELAY);
                ESP_LOGI(EXAMPLE_TAG, "Transmitted open set query\n");
                break;               
    
            default:
                break;
            }           

        } else if (action == TX_SEND_STOP_CMD) {
            //Transmit stop command to slave
            can_transmit(&stop_message, portMAX_DELAY);
            ESP_LOGI(EXAMPLE_TAG, "Transmitted stop command");
        } else if (action == TX_TASK_EXIT) {
            break;
        }
    }
    vTaskDelete(NULL);
}

void can_control_task(void *arg) {
    xSemaphoreTake(ctrl_task_sem, portMAX_DELAY);
    tx_task_action_t tx_action;
    rx_task_action_t rx_action;

    for (int iter = 0; iter < NO_OF_ITERS; iter++) {
        ESP_ERROR_CHECK(can_start());
        ESP_LOGI(EXAMPLE_TAG, "Driver started");
        while(1){
            tx_action = TX_SEND_START_CMD;
            rx_action = RX_RECEIVE_DATA;
            xQueueSend(tx_task_queue, &tx_action, portMAX_DELAY);
            xQueueSend(rx_task_queue, &rx_action, portMAX_DELAY);
            //printf("rivi 257\n");
            vTaskDelay(pdMS_TO_TICKS(PING_PERIOD_MS));
            xSemaphoreTake(ctrl_task_sem, portMAX_DELAY);
            //printf("rivi 260\n");
        }
        //Send Stop command to slave when enough data messages have been received. Wait for stop response
        xSemaphoreTake(ctrl_task_sem, portMAX_DELAY);
        tx_action = TX_SEND_STOP_CMD;
        rx_action = RX_RECEIVE_STOP_RESP;
        xQueueSend(tx_task_queue, &tx_action, portMAX_DELAY);
        xQueueSend(rx_task_queue, &rx_action, portMAX_DELAY);

        xSemaphoreTake(ctrl_task_sem, portMAX_DELAY);
        ESP_ERROR_CHECK(can_stop());
        ESP_LOGI(EXAMPLE_TAG, "Driver stopped");
        vTaskDelay(pdMS_TO_TICKS(ITER_DELAY_MS));
    }
    //Stop TX and RX tasks
    tx_action = TX_TASK_EXIT;
    rx_action = RX_TASK_EXIT;
    xQueueSend(tx_task_queue, &tx_action, portMAX_DELAY);
    xQueueSend(rx_task_queue, &rx_action, portMAX_DELAY);

    //Delete Control task
    xSemaphoreGive(done_sem);
    vTaskDelete(NULL);
}

void app_main()
{
    //Create tasks, queues, and semaphores
    rx_task_queue = xQueueCreate(1, sizeof(rx_task_action_t));
    tx_task_queue = xQueueCreate(1, sizeof(tx_task_action_t));
    ctrl_task_sem = xSemaphoreCreateBinary();
    stop_ping_sem = xSemaphoreCreateBinary();
    done_sem = xSemaphoreCreateBinary();
    xTaskCreatePinnedToCore(can_receive_task, "CAN_rx", 4096, NULL, RX_TASK_PRIO, NULL, tskNO_AFFINITY);
    xTaskCreatePinnedToCore(can_transmit_task, "CAN_tx", 4096, NULL, TX_TASK_PRIO, NULL, tskNO_AFFINITY);
    xTaskCreatePinnedToCore(can_control_task, "CAN_ctrl", 4096, NULL, CTRL_TSK_PRIO, NULL, tskNO_AFFINITY);

    //Install CAN driver
    ESP_ERROR_CHECK(can_driver_install(&g_config, &t_config, &f_config));
    ESP_LOGI(EXAMPLE_TAG, "Driver installed");

    xSemaphoreGive(ctrl_task_sem);              //Start control task
    xSemaphoreTake(done_sem, portMAX_DELAY);    //Wait for completion

    //Uninstall CAN driver
    ESP_ERROR_CHECK(can_driver_uninstall());
    ESP_LOGI(EXAMPLE_TAG, "Driver uninstalled");

    //Cleanup
    vQueueDelete(rx_task_queue);
    vQueueDelete(tx_task_queue);
    vSemaphoreDelete(ctrl_task_sem);
    vSemaphoreDelete(stop_ping_sem);
    vSemaphoreDelete(done_sem);
}
